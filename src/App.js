import React from 'react';
// import Form from "./components/Form";
// import ItemList from "./components/ItemList";
// import ListCompleted from "./components/list/ListCompleted";
// import ListActive from "./components/list/ListActive";
// import {Route} from "react-router-dom" ;
// import MenuLink from "./components/Link/MenuLink";
import styled from "styled-components";
import {compose, withState, withHandlers} from "recompose";
import {connect} from "react-redux";
import actions from "./module/todos/todosOperations";
import uuid from "uuid";

console.log(actions.addTodo)
const Div = styled.div`
    max-width: 400px;
    margin: 0 auto;
`;

const Ul = styled.ul`
    display: flex;
    justify-content: space-between;
    list-style: none;
    align-items: center;
    width: 50%;
    margin: 10px 0;
    padding: 0;
    a {
        text-decoration: none;
    }
`;

// const createList = (val) => ({
//     id: (new Date()).getTime(),
//     title: val,
//     completed: false
// });

// class App extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             list: [],
//             text: ""
//         };
//         this.handleChangeForm = this.handleChangeForm.bind(this);
//         this.handleChangeText = this.handleChangeText.bind(this);
//         this.removeItem = this.removeItem.bind(this);
//         this.handleClick = this.handleClick.bind(this);
//         console.log(this.props)
//     }
//
//     handleChangeForm(el) {
//         el.preventDefault();
//         const itemList = createList(this.state.text);
//         this.setState({
//             text: '',
//             list: [itemList].concat(this.state.list)
//         });
//     }
//
//     handleChangeText(el) {
//         this.setState({
//             text: el.target.value
//         })
//     }
//
//     removeItem(id) {
//         const {list} = this.state;
//         this.setState({
//             list: list.filter((item) => {
//                 return item.id !== id
//             })
//         })
//     }
//
//     handleClick(id) {
//         const indexOfArray = this.state.list.findIndex(i => i.id === id),
//             todo = {...this.state.list[indexOfArray]};
//
//         todo.completed = !todo.completed;
//         const newList = [...this.state.list];
//         newList[indexOfArray] = todo;
//
//         this.setState({
//             list: newList
//         })
//     }
//
//     shouldComponentUpdate(nextProps, nextState, nextContext) {
//         return nextState.list.id === this.state.list.id
//     }
//
//
//     render() {
//         console.log(this.props)
//         return (
//             <Div className="container">
//                 <Form handleChangeForm={this.props.handleAddTodo} value={this.props.text}
//                       handleChangeText={ev => this.props.handleChangeText(ev.target.value)}/>
//                 <Ul>
//                     <MenuLink to="/" onExact={true} label="Home"/>
//                     <MenuLink to="/active" label="Active"/>
//                     <MenuLink to="/complete" label="Complete"/>
//                 </Ul>
//                 <Route path="/" exact render={() => <ItemList list={this.state.list} handleClick={this.handleClick}
//                                                               removeItem={this.removeItem}/>}/>
//                 {/*{this.props.list.map(item => <div key={item.id}>{item.id} </div>)}*/}
//                 <Route path="/active" render={() => <ListActive list={this.state.list.filter(item => {
//                     return item.completed === false;
//                 })} handleClick={this.handleClick} removeItem={this.removeItem}/>}/>
//
//                 <Route path="/complete" render={() => <ListCompleted list={this.state.list.filter(item => {
//                     return item.completed === true;
//                 })} handleClick={this.handleClick} removeItem={this.removeItem}/>}/>
//
//             </Div>
//         )
//     }
// }
console.log(actions.addTodo)
function App({text, handleChangeText, handleAddTodo, addTodo}) {
    return (
        <div>
        <input type="text" onChange={(ev) => handleChangeText(ev.target.value)} value={text}/>
            <button onClick={handleAddTodo}>
                Add
            </button>
        </div>
    )
}

const mapDispatchToProps = {
    addTodo: actions.addTodo
};



const mapStateToProps = (state) => ({
    list: state.todos.todos
});

const enhancer = compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withState("text", "handleChangeText", ""),
    withHandlers({
        handleAddTodo: (props) => () => {
            const todo = {
                id: uuid(),
                text: props.text,
                completed: false
            };
            console.log(props.list)
            props.addTodo(todo);

            props.handleChangeText('');
        }
    })
);


export default enhancer(App);
