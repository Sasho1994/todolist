import React from "react";
import styled from "styled-components";
import T from "prop-types";

const Input = styled.input`
    height: 35px;
    width: 60%;
    margin-right: 20px;
    border-radius: 5px;
    box-shadow: none;
    border: 1px solid #7ac8ff;
    vertical-align: middle;    
`;
const Button = styled.button`
    background: transparent;
    font-size: 18px;
    color: #29a6ff;
    border: 1px solid #29a6ff;
    padding: 8px 15px;
    box-shadow: none;
    transition: .3s;
    vertical-align: middle;
    border-radius: 5px;
    &:hover {
        background: #29a6ff;
        color: #fff;
    }
`;

 const Form = ({handleChangeForm , value, handleChangeText}) => {
    return (
        <form onSubmit={handleChangeForm}>
            <Input type="text" value={value} onChange={handleChangeText}/>
            <Button>
                Add item
            </Button>
        </form>
    )
};

Form.T = {
    handleChangeForm: T.func,
    handleChangeText: T.func,
    value: T.string
};

export default Form;