import React from "react";
import { Ul } from "../untiles/StyledList";
import { TypeProps } from "../untiles/TypeComponents";
import T from "prop-types";
import ListCompleted from "./list/ListCompleted";

let ItemList = ({handleClick, removeItem, list}) => {
    return (
        <Ul>
            {list.map(i => {
                return (
                    <li key={i.id} onClick={() => handleClick(i.id)}>
                        <span>
                            {i.title}
                        </span>
                        <button onClick={(el) => {
                            el.stopPropagation();
                            removeItem(i.id)
                        }}>
                            &#10006;
                        </button>
                    </li>
                )
            })}
        </Ul>
    )
};

ListCompleted.T = TypeProps(T);

export default ItemList;