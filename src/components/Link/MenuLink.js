import React from "react";
import { Route, Link } from "react-router-dom";
import styled from "styled-components";
import T from "prop-types";

const StyledLink = styled(Link) `
    display: inline-block;
`;

const Li = styled.li `
    display: flex;
    justify-content: space-between;
`;

const style = (el) => {
    if (!el) {
        return {
            color: "#000",
        }
    } else {
        return {
            color: "#29a6ff",
            textDecoration: "underline"
        }
    }
};

const MenuLink = ({label, to,  onExact}) => {
    return (
        <Route
            path={to}
            exact={onExact}
            children={({match}) =>
                <Li>
                    <StyledLink to={to} style={style(match)}>
                        {label}
                    </StyledLink>
                </Li>
            }
        >
        </Route>
    );
};

MenuLink.T = {
    label: T.string,
    to: T.string,
    onExact: T.bool
};

export default MenuLink;



