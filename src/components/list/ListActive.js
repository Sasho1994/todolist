import React from "react";
import { Ul } from "../../untiles/StyledList";
import { TypeProps } from "../../untiles/TypeComponents";
import T from "prop-types";
import ListCompleted from "./ListCompleted";


const ListActive = ({list, handleClick, removeItem}) => {
    return (
        <Ul>
            {list.map(item => {
                return (
                    <li key={item.id} onClick={() => handleClick(item.id)}>
                        <span>{item.title}</span>
                        <button onClick={(el) => {
                            el.stopPropagation();
                            removeItem(item.id)
                        }}>
                            &#10006;
                        </button>
                    </li>
                )
            })}
        </Ul>
    )
};

ListCompleted.T = TypeProps(T);

export default ListActive;