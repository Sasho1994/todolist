import { createActions } from "redux-actions";

export const addTodo = createActions('todos/ADD_TODO');