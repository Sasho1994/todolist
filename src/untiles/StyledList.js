import styled from "styled-components";

export const Ul = styled.ul `
  list-style: none;
  padding: 0;
  li {
    display: flex;
    justify-content: space-between;
  }
  span {
    cursor: pointer;
    transition: .3s;
    &:hover {
        color: #7ac8ff;
    }    
  }
  button {
    font-size: 20px;
    color: #000;
    background: transparent;
    box-shadow: none;
    border: none;
    cursor: pointer;
    transition: .3s;
    &:hover {
        color: #7ac8ff;
    }
  } 
`;