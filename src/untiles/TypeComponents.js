export const TypeProps = (type) => {
   return {
       list: type.array,
       removeItem: type.func,
       handleClick: type.func
   }
};